# Graph charts with labels
This style creates a bar chart with two bars for each point and adds a label with the value for each bar above the bar, similar like graph charts in spreadsheets.  The same result can also be more or less achieved without geometry generators, but that way it's more difficult to place the labels at a consistent distance fromt the bar. In the project file both versions can be compared.

Since the example point layer only has one column with example values a second bar is created as "value/2+10".

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/graph_charts_with_labels/graph_charts_with_labels.qml?inline=false"><img src="../../Example_images/graph_charts_with_labels.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/graph_charts_with_labels/graph_charts_with_labels.qml?inline=false)
